function validate(e) {
    let validFirstName = true;
    let validLastName = true;
    let validMail = true;
    const firstNameField = document.getElementById("firstname");
    const lastNameField = document.getElementById("lastname");
    const mailField = document.getElementById("mail");

    let mailregex = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})$/;


    if (firstNameField.value.length < 4) {
        validFirstName = false;
        firstNameField.setAttribute("class", "error");
    } else {
        firstNameField.removeAttribute("class");
    }

    if (lastNameField.value.length == 0) {
        validLastName = false;
        lastNameField.setAttribute("class", "error");
    } else {
        lastNameField.removeAttribute("class");
    }

    if (!mailregex.test(mailField.value)) {
        validMail = false;
        mailField.setAttribute("class", "error");
    }
    else {
        mailField.removeAttribute("class");
    }

    
    

    
    


    if (!validLastName) {
        const errorElement = document.getElementById("lastNameError");
        errorElement.innerHTML = "Required field";
        e.preventDefault();
    }
    if (!validFirstName) {
        const errorElement = document.getElementById("firstNameError");
        errorElement.innerHTML = "Must contain 3 characters";
        e.preventDefault();
    }
    if (!validMail) {
        const errorElement = document.getElementById("mailError");
        errorElement.innerHTML = "Enter a valid email adress";
        e.preventDefault();
    }
   

}

function reset(e) {
    document.getElementById("firstNameError").innerHTML = "";
    document.getElementById("lastNameError").innerHTML = "";
    document.getElementById("mailError").innerHTML = "";
    document.getElementById("birthdayError").innerHTML = "";
    document.getElementById("firstname").removeAttribute("class");
    document.getElementById("lastname").removeAttribute("class");
    document.getElementById("mail").removeAttribute("class");
    document.getElementById("comp").innerHTML = "";

}

function init() {
    const form = document.getElementById("myForm");
    form.addEventListener("submit", validate);
    form.addEventListener("reset", reset);
    
    
}
var audio = new Audio('yes-2.wav');
function myAudioFunction(){
    audio.play();
    


}



function myVisualFunction(){
    document.getElementById("vink").style.visibility='visible';
}

window.addEventListener("load", init);